<?php
require_once 'interfaces/Workable.php';
require_once 'classes/Project.php';
require_once 'classes/HourlyProject.php';
require_once 'classes/MilestoneProject.php';
require_once 'classes/Task.php';
require_once 'classes/ProjectFeedWriter.php';

$hourlyProject = new HourlyProject();

$milestoneProject = new MilestoneProject();

$task = new Task();

$html = new ProjectFeedWriter($hourlyProject);
$html2 = new ProjectFeedWriter($milestoneProject);
$html3 = new ProjectFeedWriter($task);

echo $html->htmlWriter();
echo $html2->htmlWriter();
echo $html3->htmlWriter();




