<?php


abstract class Project implements Workable
{
    protected $title;
    protected $description;

    abstract public function getPrice();

    abstract public function getProjectProgress();
}