<?php


class HourlyProject extends Project
{
    protected $allHours = 20;
    protected $currentHours = 5;
    protected $pricePerHour = 3;

    public function getTitle()
    {
        return $this->title = 'Проект с почасвой оплатой';
    }

    public function getDescription()
    {
        return $this->description = 'Проект займет ' . $this->allHours . ' часов. На данный момент сделано: ' . $this->getProjectProgress() . ' % работы.';
    }

    public function getPrice()
    {
        return $this->allHours * $this->pricePerHour . '$';
    }

    public function getProjectProgress()
    {
        $price = $this->currentHours * 100 / $this->allHours;
        return round($price, 2);
    }
}