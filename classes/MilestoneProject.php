<?php


class MilestoneProject extends Project
{
    protected $totalStages = 12;
    protected $currentStage = 4;
    protected $priceProject = 80;

    public function getPrice()
    {
        return $this->priceProject . '$';
    }

    public function getProjectProgress()
    {
        $stage = $this->currentStage * 100 / $this->totalStages;
        return round($stage, 2);
    }

    public function getTitle()
    {
        return $this->title = 'Поэтапный проект';
    }

    public function getDescription()
    {
        return $this->description = 'Проект займет ' . $this->totalStages . ' этапов. На данный момент сделано: ' . $this->getProjectProgress() . ' % работы.';
    }
}