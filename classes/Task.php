<?php


class Task implements Workable
{
    protected $title = 'Задача';
    protected $description = 'Сделать чай';
    protected $price = 150;

    public function getPrice()
    {
        return $this->price . '$';
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }
}