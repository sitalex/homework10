<?php


class ProjectFeedWriter
{
    protected $projects;

    public function __construct(Workable $project)
    {
        $this->projects = $project;
    }

    public function htmlWriter()
    {
        $html = '';
        $html .= '<ul>';
        $html .= '<li>' . 'Название:' .$this->projects->getTitle() . '</li>';
        $html .= '<li>' . 'Описание:' . $this->projects->getDescription() . '</li>';
        $html .= '<li>' . 'Цена:' . $this->projects->getPrice() . '</li>';
        $html .= '</ul>';

        return $html;
    }
}